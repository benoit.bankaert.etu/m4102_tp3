package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.AssociationCreateDto;
import fr.ulille.iut.pizzaland.dto.AssociationDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.util.Objects;

public class Association {
    private long pId;
    private long iId;

    public Association() {
    }

    public Association(long pId, long iId) {
        this.pId = pId;
        this.iId = iId;
    }

    public void setPid(long id) {
        this.pId = id;
    }

    public long getPId() {
        return pId;
    }

    public long getiId() {
        return iId;
    }

    public void setiId(long iId) {
        this.iId = iId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Association that = (Association) o;
        return pId == that.pId &&
                iId == that.iId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pId, iId);
    }

    @Override
    public String toString() {
        return "Association [pId=" + pId + ", iId=" + iId + "]";
    }

    public static AssociationCreateDto toCreateDto(Association association) {
        AssociationCreateDto dto = new AssociationCreateDto();
        dto.setiId(association.getiId());
        dto.setpId(association.getPId());

        return dto;
    }

    public static Association fromAssociationCreateDto(AssociationCreateDto dto) {
        Association association = new Association();
        association.setPid(dto.getpId());
        association.setiId(dto.getiId());

        return association;
    }

    public static AssociationDto toDto(Association a) {
        AssociationDto dto = new AssociationDto();
        dto.setpId(a.getPId());
        dto.setiId(a.getiId());

        return dto;
    }

    public static Association fromDto(AssociationDto dto) {
        Association association = new Association();
        association.setiId(dto.getiId());
        association.setPid(dto.getpId());

        return association;
    }
}
