package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.beans.Association;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id INTEGER PRIMARY KEY,name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (pId INTEGER,iId INTEGER, PRIMARY KEY(pId,iId),FOREIGN KEY(pId) REFERENCES Pizzas(id),FOREIGN KEY(iId) REFERENCES ingredients(id))")
    void createAssociationTable();

    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAllPizza();

    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findPizzaById(long id);

    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findPizzaByName(String name);

    @SqlUpdate("INSERT INTO Pizzas (name) VALUES (:name)")
    @GetGeneratedKeys
    long insertPizza(String name);

    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropPizzaTable();

    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void removePizza(long id);

    @SqlQuery("SELECT * FROM Association")
    @RegisterBeanMapper(Association.class)
    List<Association> getAllAssociation();

    @SqlQuery("SELECT * FROM Association WHERE pId = :id")
    @RegisterBeanMapper(Association.class)
    Association findAssociationBypId(long id);

    @SqlQuery("SELECT * FROM Association WHERE iId = :id")
    @RegisterBeanMapper(Association.class)
    Association findAssociationByiId(long id);


    @SqlUpdate("INSERT INTO Association (pId,iId) VALUES (:pId,:iId)")
    @GetGeneratedKeys
    long insertAssociation(long pId,long iId);

    @SqlUpdate("DROP TABLE IF EXISTS Association")
    void dropAssociationTable();

    @SqlUpdate("DELETE FROM Association WHERE pId = :pId")
    void removeAssiociationBypId(long pId);

    @SqlUpdate("DELETE FROM Association WHERE iId = :iId")
    void removeAssiociationByiId(long iId);

    @Transaction
    default void createTableAndIngredientAssociation() {
        createAssociationTable();
        createPizzaTable();
    }
}
