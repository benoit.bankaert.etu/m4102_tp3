package fr.ulille.iut.pizzaland.dto;

public class AssociationCreateDto {
    private long pId;
    private long iId;

    public AssociationCreateDto() {}

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public long getiId() {
        return iId;
    }

    public void setiId(long iId) {
        this.iId = iId;
    }


}
