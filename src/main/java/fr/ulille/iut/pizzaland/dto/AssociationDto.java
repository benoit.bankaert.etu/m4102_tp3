package fr.ulille.iut.pizzaland.dto;

public class AssociationDto {

    private long pId;
    private long iId;


    public AssociationDto(){}

    public long getpId() {
        return pId;
    }

    public void setiId(long iId) {
        this.iId = iId;
    }

    public long getiId() {
        return iId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }
}
