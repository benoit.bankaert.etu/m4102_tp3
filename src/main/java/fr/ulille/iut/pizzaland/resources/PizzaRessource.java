package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/pizzas")
public class PizzaRessource {
    private static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());

    @Context
    public UriInfo uriInfo;

    private PizzaDao pizzaDao;

    public PizzaRessource() {
        pizzaDao = BDDFactory.buildDao(PizzaDao.class);
        pizzaDao.createPizzaTable();
        pizzaDao.createAssociationTable();
    }

    @GET
    public List<PizzaDto> getAll() {
        LOGGER.info("PizzaRessource:getAll");
        List<PizzaDto> l = pizzaDao.getAllPizza().stream().map(Pizza::toDto).collect(Collectors.toList());
        return l;
    }

    @GET
    @Path("{id}")
    public PizzaDto getOnePizza(@PathParam("id") long id) {
        LOGGER.info("getOnePizza(" + id + ")");
        try {
            Pizza pizza = pizzaDao.findPizzaById(id);
            return Pizza.toDto(pizza);
        }
        catch ( Exception e ) {
            // Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @POST
    public Response createPizza(PizzaCreateDto pizzaCreateDto) {
        Pizza existing = pizzaDao.findPizzaByName(pizzaCreateDto.getName());
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizza = Pizza.fromIngredientCreateDto(pizzaCreateDto);
            long id = pizzaDao.insertPizza(pizza.getName());
            pizza.setId(id);
            PizzaDto pizzaDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pizzaDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

    @DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") long id) {
        if ( pizzaDao.findPizzaById(id) == null ) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        pizzaDao.removePizza(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }

    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") long id) {
        Pizza pizza = pizzaDao.findPizzaById(id);
        if ( pizza == null ) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return pizza.getName();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createPizza(@FormParam("name") String name) {
        Pizza existing = pizzaDao.findPizzaByName(name);
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizza = new Pizza();
            pizza.setName(name);

            long id = pizzaDao.insertPizza(pizza.getName());
            pizza.setId(id);
            PizzaDto pizzaDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pizzaDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
}
