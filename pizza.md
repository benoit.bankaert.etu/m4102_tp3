Utilisation de l'api pour les pizzas

| Operation | URI               | Action Realisee                       | Retour                     |
|-----------|-------------------|---------------------------------------|----------------------------|
| GET       | /pizzas           | Recupère la liste de toute les pizzas | 200 et un tableau de pizza |
| GET       | /pizzas/{id}      | Recupère la pizza d'ID = id           | 200 et la pizza            |
|           |                   |                                       | 401 si id inconnu          |
| GET       | /pizzas/{id}/name | Recupère le nom de la pizza n°ID      | 200 et le nom de la pizza  |
|           |                   |                                       | 401 si id inconnu          |